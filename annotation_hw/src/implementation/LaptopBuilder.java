package implementation;//This interface defines the methods needed to create the parts of the Laptop

import implementation.Laptop;

public interface LaptopBuilder {

    public void buildScreenSize();

    public void buildProcessor();

    public void buildMemory();

    public void buildRAM();

    public void buildGraphicsCard();

    //This method gets the Laptop (Product) that is build by the Builder (Director)

    public Laptop getLaptop();

}
