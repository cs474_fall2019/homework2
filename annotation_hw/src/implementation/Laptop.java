//This is the concrete Camera class based on the LaptopBlueprint Interface

package implementation;

public class Laptop implements LaptopBlueprint {

    private double screenSize;
    private String processor;
    private String memory;
    private String ram;
    private String graphicsCard;

    @Override
    public void setScreenSize(float size) {

        this.screenSize = size;

    }

    public double getScreenSize() {

        return screenSize;
    }

    @Override
    public void setProcessor(String processor) {

        this.processor = processor;

    }

    public String getProcessor() {

        return processor;
    }

    @Override
    public void setMemory(String memory) {

        this.memory = memory;

    }

    public String getMemory() {

        return memory;
    }

    @Override
    public void setRAM(String ram) {

        this.ram = ram;

    }

    public String getRAM() {

        return ram;
    }

    @Override
    public void setGraphicsCard(String graphicsCard) {

        this.graphicsCard = graphicsCard;

    }

    public String getGraphicsCard() {

        return graphicsCard;
    }

    // Function to display all the specifications of the built Laptop

    public void showLaptop() {

        System.out.println("Laptop built...");

        System.out.println("Laptop Screen Size: " + getScreenSize());

        System.out.println("Laptop Processor: " + getProcessor());

        System.out.println("Laptop Memory: " + getMemory());

        System.out.println("Laptop RAM: " + getRAM());

        System.out.println("Laptop Graphics Card: " + getGraphicsCard());

    }

}