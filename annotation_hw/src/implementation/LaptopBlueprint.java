package implementation;//This interface will be returned from the LaptopBuilder class

public interface LaptopBlueprint {

    public void setScreenSize(float size);

    public void setProcessor(String processor);

    public void setMemory(String memory);

    public void setRAM(String ram);

    public void setGraphicsCard(String graphicsCard);

}