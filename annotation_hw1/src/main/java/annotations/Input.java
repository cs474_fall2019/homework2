// This is the custom annotation of element TYPE

package annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Input {
    double screenSize() default 15.6;
    String processor() default "Intel i7";
    String memory() default "256GB SSD";
    String ram() default "8GB LPDDR3";
    String graphicsCard() default "NVIDIA GTX1020";
}
