import annotations.Input;
import org.testng.annotations.Test;

import java.lang.annotation.Annotation;

public class test2 {
    Annotation annotation = Test.class.getAnnotation(Input.class);
    if(annotation instanceof Input) {
        Input input = (Input) annotation;

        assertequals(15.6, info.screenSize());
        assertequals("Intel i7", info.processor());
        assertequals("256GB SSD", info.memory());
        assertequals("16GB LPDDR4", info.ram());
        assertequals("NVIDIA GTX1020", info.graphicsCard());
    }
    else {
        fail("Did not find the annotation");
    }


}
